import click
from model import PostManager, PostModel


def post_create_view(post: PostModel):
    click.echo(f"Created {str(post)} successfully")


def post_read_view(post: PostModel):
    click.echo(str(post))


def post_update_view(post: PostModel):
    click.echo(f"Updated {str(post)} successfully")


def post_delete_view():
    click.echo("Deleted successfully")


def post_list_view(manager: PostManager):
    click.echo(manager.objects)
