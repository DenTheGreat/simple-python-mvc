from .view import (
    post_create_view,
    post_list_view,
    post_read_view,
    post_update_view,
    post_delete_view,
)
from model import HaventFoundError, PostModel, PostManager
import click


@click.command()
@click.option(
    "-o",
    "--operation",
    type=click.Choice(
        ["create", "c", "read", "r", "update", "u", "delete", "d", "list", "l"]
    ),
    help="Операция CRUD",
)
@click.option("--id", type=click.INT, help="id")
@click.option("--text", type=click.STRING, help="text")
@click.option("--title", type=click.STRING, help="title")
def post_controller(operation, id, text, title):
    objects = PostManager()
    obj: PostModel
    try:
        if operation == "create" or operation == "c":
            obj = objects.create(PostModel(text=text, title=title))
            post_create_view(obj)
        elif operation == "read" or operation == "r":
            obj = objects.read(id)
            post_read_view(obj)
        elif operation == "update" or operation == "u":
            obj = objects.update(PostModel(id=id, text=text, title=title))
            post_update_view(obj)
        elif operation == "delete" or operation == "d":
            obj = objects.delete(id)
            post_delete_view()
        elif operation == "list" or operation == "l":
            post_list_view(objects)
        else:
            raise click.Abort()
    except HaventFoundError:
        raise click.ClickException("Такой объект не найден или не хватает параметров")


