import PySimpleGUI as sg

sg.theme("Dark Blue 3")

layout_create = [
    [sg.Text("Create")],
    [sg.Input(key="-CREATE-TITLE-", default_text="Title")],
    [sg.Input(key="-CREATE-TEXT-", default_text="Text")],
    [sg.Button("Save", key="-CREATE-SAVE-")],
]
layout_read = [
    [sg.Text("Read")],
    [
        sg.Column(
            [
                [sg.Input(key="-READ-ID-", default_text="Id")],
                [sg.Button("Find", key="-READ-")],
            ],
            key="-READ-GET-",
        ),
        sg.Column(
            [
                [
                    sg.Text(size=(15, 1), key="-OUTPUT-TITLE-"),
                    sg.Text(key="-OUTPUT-ID-"),
                ],
                [sg.Text(size=(15, 1), key="-OUTPUT-TEXT-")],
                [sg.Button("Close", key="-READ-CLOSE-")],
            ],
            visible=False,
            key="-READ-SHOW-",
        ),
    ],
]
layout_update = [
    [sg.Text("Update")],
    [sg.Input(key="-UPD-ID-", default_text="Id")],
    [sg.Input(key="-UPD-TITLE-", default_text="Title")],
    [sg.Input(key="-UPD-TEXT-", default_text="Text")],
    [sg.Button("Save", key="-UPD-SAVE-")],
]
layout_delete = [
    [sg.Text("Delete")],
    [sg.Input(key="-DEL-ID-", default_text="Id")],
    [sg.Button("DELETE", key="-DELETE-")],
]


layout = [
    [
        sg.Column(
            [
                [sg.Button("Create")],
                [sg.Button("Read")],
                [sg.Button("Update")],
                [sg.Button("Delete")],
            ]
        ),
        sg.Column(layout_create, visible=False, key="-COL-Create-"),
        sg.Column(layout_read, visible=False, key="-COL-Read-"),
        sg.Column(layout_update, visible=False, key="-COL-Update-"),
        sg.Column(layout_delete, visible=False, key="-COL-Delete-"),
    ]
]
