import PySimpleGUI as sg

from model import PostManager, PostModel


def gui_controller():
    from .view import layout

    window = sg.Window("CRUD MVC", layout)

    objects = PostManager()
    layout = None
    while True:
        event, values = window.read()
        # print(event, values)
        if event in (None, "Exit"):
            break
        elif event in ["Create", "Read", "Update", "Delete"]:
            if layout:
                window[f"-COL-{layout}-"].update(visible=False)
            layout = event
            window[f"-COL-{layout}-"].update(visible=True)
        elif event == "-CREATE-SAVE-":
            post = PostModel(
                title=values["-CREATE-TITLE-"], text=values["-CREATE-TEXT-"]
            )
            objects.create(post)
            window[f"-COL-Create-"].update(visible=False)
        elif event == "-READ-":
            post = objects.read(id=int(values["-READ-ID-"]))
            window[f"-READ-GET-"].update(visible=False)
            window[f"-READ-SHOW-"].update(visible=True)
            window[f"-OUTPUT-TITLE-"].update(post.title)
            window[f"-OUTPUT-TEXT-"].update(post.text)
            window[f"-OUTPUT-ID-"].update(str(post.id))
        elif event == "-READ-CLOSE-":
            window[f"-READ-GET-"].update(visible=True)
            window[f"-READ-SHOW-"].update(visible=False)
            window[f"-COL-Read-"].update(visible=False)
        elif event == "-UPDATE-SAVE-":
            post = PostModel(
                title=values["-UPD-TITLE-"],
                text=values["-UPD-TEXT-"],
                id=values["-UPD-ID-"],
            )
            objects.update(post)
            window[f"-COL-Update-"].update(visible=False)
        elif event == "-DELETE-":
            objects.delete(id=int(values["-DEL-ID-"]))
            window[f"-COL-Delete-"].update(visible=False)
    window.close()