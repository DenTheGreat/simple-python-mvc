import pickle
from typing import List, Optional, Tuple
from pydantic import BaseModel


class HaventFoundError(Exception):
    def __init__(self):
        self.expression = "Такой объект не найден"


class PostModel(BaseModel):
    id: Optional[int]
    title: str
    text: Optional[str]


class PostManager:
    objects: List[PostModel]

    def __init__(self) -> None:
        try:
            with open("data.pickle", "rb") as f:
                self.objects = pickle.load(f)
        except:
            self.objects = []

    def _save(self):
        with open("data.pickle", "wb") as f:
            pickle.dump(self.objects, f)

    def _get_item(self, id: int) -> Tuple[PostModel, int]:
        obj_idx = [idx for idx, item in enumerate(self.objects) if item.id == id]
        if len(obj_idx) == 0:
            raise HaventFoundError
        else:
            return self.objects[obj_idx[0]], obj_idx[0]

    def create(self, object: PostModel) -> PostModel:
        object.id = 1 if len(self.objects) == 0 else len(self.objects) + 1
        self.objects.append(object)
        self._save()
        return object

    def update(self, object: PostModel) -> PostModel:
        _, idx = self._get_item(object.id)
        self.objects[idx] = object
        self._save()
        return object

    def delete(self, id: int):
        _, idx = self._get_item(id)
        self.objects.pop(idx)
        self._save()

    def read(self, id: int) -> PostModel:
        obj, _ = self._get_item(id)
        return obj